# Logic Agents Exercises

## Project structure

    .                           # current dir
    ├── artefacts               # `artefacts` module (exercise 3)
    │   ├── build.gradle.kts
    │   └── src
    ├── build.gradle.kts        # top-level gradle configuration script
    ├── gradle                  # gradle runtime, do not delete
    │   └── wrapper
    ├── gradle.properties       # default gradle properties, do not delete
    ├── gradlew                 # build script to be run Linux and MacOS
    ├── gradlew.bat             # build script to be run Windows
    ├── helloworld              # `helloworld` module (exercise 1)
    │   ├── build.gradle.kts
    │   └── src
    ├── README.md               # this file
    ├── settings.gradle         # gradle modules declarations
    ├── thermostat              # `thermostat` module (exercise 2)
    │   ├── build.gradle.kts
    │   └── src
    └── vacuum                  # `vacuum` module (exercise 4)
        ├── build.gradle.kts
        └── src

You can run each exercise/module from a shell opened within the `.` directory, by running:

```bash
./gradlew :<module name>:run
```

where colons are part of the command.
The name of each module corresponds to the name of the directory containing it.

### Example

By running:

```bash
./gradlew :helloworld:run
```

one may execute the `./helloworld/src/main/java/it/unibo/as/logicagents/Main.java` class (which will be automatically compiled).
    
This is because the `./helloworld/src/build.gradle.kts` states that:
    

```kotlin
application {
    mainClassName = "it.unibo.as.logicagents.Main"
}
```

## Running 2p

The top-level gradle configuration script let users start a tuProlog engine using either the command line interface or the graphical one.

To start the command line interface, one must simply execute the following command:

```bash
./gradlew 2p -Ptheory=path/to/theory/file.pl
```

Gradle will automatically take care of downloading `tuProlog 3.3.0` from [its Bitbucket download page](https://bitbucket.org/tuprologteam/tuprolog/downloads/2p-3.3.0.zip),
and unpacking it into the `./tuprolog/` folder.

The provided theory file will be loaded upon Prolog engine instantiation.
If no `-Ptheory=...` property is provided, the `./helloworld/src/main/resources/Hello.pl` theory is automatically loaded.

To start the GUI, one must simply execute the following command:

```bash
./gradlew 2p-gui
```

(no default theory is loaded in this case)

Finally, if one just want to download `tuProlog`, he / she must simply run the command:

```bash
./gradlew download2p
```

which downloads the zipped distribution into the `./tuprolog/2p.zip`.

The `./gradlew unzip2p` command may then be used to automatically unpack the downloaded archive into `./tuprolog/`.


## Modules structure

Each exercise consist of a Gradle module (or sub-project) having the following structure:

    <module directory>
    ├── build.gradle.kts                            # gradle configuration script for this module
    └── src
        └── main
            ├── java
            │   └── <Java sources and packages>
            └── resources
                └── <Prolog sources>

## Exercise 4 – Rationale

The exercise requires the students to develop the logic control software for a vacuum moving into a 2D discrete arena
where some cells are dirty.
Upon startup, the agent has no idea of where it is, what is its orientation into the arena, or which and how many cells are dirty.
By only leveraging on its reasoning capabilities and its artifacts, it must:

1. understand the size of the arena it is moving into

2. clean it all

To do so, it can exploit a motor actuator, an odometric sensor, a dirty scanner, and a sucker actuator.

### Solution description

Since the vacuum agent has no idea of where it is when it start, it assumes its initial position to be the origin of its own
reference frame, and the direction it is initially looking to to be called "top".
The only thing the agent knows/assumes is that the arena is rectangular in shape.
It will then explore the arena until reaching the top border (according to ITS reference frame).
After that, it will start wondering on the arena border (clockwise or anti-clockwise) until it is sure of the arena size.
Finally, it will start exaustively navigate the arena, eventually cleaning up all the dirty cells.

(Notice that this is just ONE possible solution, not necessarily the best one.)

### Environment representation

The current implementation represent the system environment on the console, as follows:

    ____________________
    X__X___X_X_XXXXXXXX_
    _XXXX__X_XXX_X_XXXX_
    _X_XXXXXX_XX_XXXXX__
    _X_XX__X_X_XXXX__XX_
    __X_________________
    __X___X_XXX__XXXXX_X
    ___→__XXXXXXXXXXXXXX
    ____________________
    ____________________

There, an underscore character represents an empty cell, whereas `X`s represent dirty cells.
The arrow represents the vacuum position and bearing.

Whereas the source file to be completed by students is `./logic-agents/vacuum/src/main/resources/Vacuum.pl`, an utility
file is provided (namely, `./logic-agents/vacuum/src/main/resources/Utils.pl`) containing several Prolog rules which may
be useful while implementing a solution.