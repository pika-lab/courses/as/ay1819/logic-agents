package it.unibo.as.logicagents;

import alice.tuprolog.InvalidTheoryException;
import alice.tuprolog.MalformedGoalException;
import alice.tuprolog.Prolog;
import alice.tuprolog.Theory;
import alice.tuprolog.event.ExceptionEvent;
import alice.tuprolog.event.OutputEvent;

import java.io.IOException;
import java.io.InputStream;

public class Main {
    public static void main(String[] args) throws InvalidTheoryException, MalformedGoalException, IOException {
        Prolog agent = createEngineWithTheory(Main.class.getResourceAsStream("/Hello.pl"));

        agent.solve("start.");
    }

    public static Prolog createEngineWithTheory(InputStream theory) throws InvalidTheoryException, IOException {
        return createEngineWithTheory(new Theory(theory));
    }

    public static Prolog createEngineWithTheory(Theory theory) throws InvalidTheoryException {
        Prolog engine = new Prolog();
        engine.setTheory(theory);
        engine.addOutputListener(Main::printOutput);
        engine.addExceptionListener(Main::printError);
        return engine;
    }

    private static void printOutput(OutputEvent object) {
        System.out.print(cleanUpMessage(object.getMsg()));
    }

    private static void printError(ExceptionEvent object) {
        System.err.print(cleanUpMessage(object.getMsg()));
    }

    private static String cleanUpMessage(String msg) {
        if (msg.startsWith("'") && msg.endsWith("'")) {
            return msg.substring(1, msg.length() - 1);
        }
        return msg;
    }
}
