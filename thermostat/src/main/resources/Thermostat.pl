temp(25).

start :- check_temperature.

check_temperature :-
    temp(X), X >= 22, !,
    println(["Temperature too high: ", X, ", cooling down"]),
    T is X - 1,
    change_temperature(T).
check_temperature :-
    temp(X), X =< 18, !,
    println(["Temperature too low: ", X, ", warming up"]),
    T is X + 1,
    change_temperature(T).
check_temperature :-
    temp(X), X > 18, X < 22, !,
    println(["Temperature is ok: ", X, ", I''m done"]).

change_temperature(X) :-
    retract(temp(_)),
    assert(temp(X)), !,
    check_temperature.


println([]) :- nl.
println([X | Xs]) :- write(X), println(Xs).