package it.unibo.as.logicagents;

import java.util.Random;

/**
 * An integer 2D position
 */
public class Position {

    private static Random RAND = new Random();

    private final int x;
    private final int y;

    public static Position of(int x, int y) {
        return new Position(x, y);
    }

    public static Position randomIn(int width, int height) {
        return new Position(RAND.nextInt(width) + 1, RAND.nextInt(height) + 1);
    }

    private Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Position)) return false;

        Position position = (Position) o;

        if (getX() != position.getX()) return false;
        return getY() == position.getY();
    }

    public Position inc(int delta, Bearing bearing) {
        switch (bearing) {
            case NORTH: return Position.of(x, y - delta);
            case SOUTH: return Position.of(x, y + delta);
            case EAST: return Position.of(x + delta, y);
            case WEST: return Position.of(x - delta, y);
            default: throw new IllegalStateException();
        }
    }

    public Position inc(Bearing bearing) {
        return inc(1, bearing);
    }

    @Override
    public int hashCode() {
        int result = getX();
        result = 31 * result + getY();
        return result;
    }

    @Override
    public String toString() {
        return String.format("(%d, %d)", x, y);
    }
}
