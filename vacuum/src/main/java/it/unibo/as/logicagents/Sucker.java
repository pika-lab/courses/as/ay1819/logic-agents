package it.unibo.as.logicagents;

/**
 * This is an actuator artifact letting the agent clean up the environment position it is lying upon
 */
public class Sucker extends Artefact {

    public Sucker(VacuumEnvironment environment) {
        super(environment);
    }

    /**
     * Tries to clean up the environment position the agent is lying upon.
     * Fails in case that position is still dirty after the cleaning attempt
     *
     * @return a <code>boolean</code> value
     * @throws Exception
     */
    public boolean cleanUp() throws Exception {
        final Position position = getEnvironment().getVacuumPosition();

        // TODO do something
        throw new IllegalStateException("Implement me");

//        return !getEnvironment().isDirty(position);
    }
}
