package it.unibo.as.logicagents;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * A 2D, discrete environment for the vacuum agent.
 * It keeps track of:
 * <ul>
 *     <li>the arena size</li>
 *     <li>the vacuum {@link Position} within the arena (1-based indexes)</li>
*      <li>the vacuum {@link Bearing} within the arena</li>
 *     <li>the {@link Position} of the dirty cells</li>
 * </ul>
 * plus, it provides a number of handy methods for checking or manipulating the environment.
 *
 * Notice that this class is to be instantiated as an anonymous class, possibly overriding one or more of the three callbacks it exposes
 */
public abstract class VacuumEnvironment {

    private final int width;
    private final int height;
    private final Set<Position> dirty = new HashSet<>();

    private Position vacuumPosition;
    private Bearing vacuumBearing;

    private VacuumEnvironment(int width, int height, Position vacuumPosition, Bearing bearing, Position... dirty) {
        this.width = width;
        this.height = height;
        this.vacuumPosition = Objects.requireNonNull(vacuumPosition);
        if (!isWithin(vacuumPosition)) {
            throw new IllegalArgumentException("Position " + vacuumPosition + " is out of the environment. Please use 1-based indexes");
        }
        this.vacuumBearing = Objects.requireNonNull(bearing);
        this.dirty.addAll(Stream.of(dirty).collect(Collectors.toSet()));
        this.onInit();
        this.onEnvironmentChanged();
        this.onVacuumMoved();
    }

    public VacuumEnvironment(int width, int height, int x, int y, Bearing bearing, Position... dirty) {
        this(width, height, Position.of(x, y), bearing, dirty);
    }

    public VacuumEnvironment(int width, int height, Position... dirty) {
        this(width, height, Position.randomIn(width, height), Bearing.random(), dirty);
    }

    /**
     * Callback called upon environment's initialisation
     */
    protected void onInit() {

    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void cleanUp(Position position) {
        if (!isWithin(position)) {
            throw new IllegalArgumentException("Position " + position + " is out of the environment. Please use 1-based indexes");
        }
        dirty.remove(Objects.requireNonNull(position));
        this.onEnvironmentChanged();
    }

    public void cleanUp(int x, int y) {
        cleanUp(Position.of(x, y));
    }

    public void setDirty(Position position) {
        if (!isWithin(position)) {
            throw new IllegalArgumentException("Position " + position + " is out of the environment. Please use 1-based indexes");
        }
        dirty.add(Objects.requireNonNull(position));
        this.onEnvironmentChanged();
    }

    public void setDirty(int x, int y) {
        setDirty(Position.of(x, y));
    }

    public void setRandomlyDirty() {
        setDirty(Position.randomIn(width, height));
    }

    public boolean isDirty(Position position) {
        return dirty.contains(Objects.requireNonNull(position));
    }

    public boolean isDirty(int x, int y) {
        return dirty.contains(Position.of(x, y));
    }

    public Position getVacuumPosition() {
        return vacuumPosition;
    }

    public void setVacuumPosition(Position vacuumPosition) {
        Position previous = this.vacuumPosition;
        if (!isWithin(vacuumPosition)) {
            throw new IllegalArgumentException("Position " + vacuumPosition + " is out of the environment. Please use 1-based indexes");
        }
        this.vacuumPosition = Objects.requireNonNull(vacuumPosition);
        if (!previous.equals(this.vacuumPosition)) {
            this.onEnvironmentChanged();
            this.onVacuumMoved();
        }
    }

    public void setVacuumPosition(int x, int y) {
        setVacuumPosition(Position.of(x, y));
    }

    public Bearing getVacuumBearing() {
        return vacuumBearing;
    }

    public void setVacuumBearing(Bearing vacuumBearing) {
        Bearing previous = this.vacuumBearing;
        this.vacuumBearing = Objects.requireNonNull(vacuumBearing);
        if (!previous.equals(this.vacuumBearing)) {
            this.onEnvironmentChanged();
            this.onVacuumMoved();
        }
    }

    public boolean isWithin(int x, int y) {
        return x >= 1 && x <= width
                && y >= 1 && y <= height;
    }

    public boolean isWithin(Position position) {
        return isWithin(position.getX(), position.getY());
    }

    @Override
    public String toString() {
        StringBuilder envStr = new StringBuilder();
        for (int r = 0; r < height; r++) {
            for (int c = 0; c < width; c++) {
                if (getVacuumPosition().equals(Position.of(c + 1, r + 1))) {
                    envStr.append(getVacuumBearing().toString());
                } else if (isDirty(c + 1, r + 1)) {
                    envStr.append("X");
                } else {
                    envStr.append("_");
                }
            }
            envStr.append("\n");
        }
        return envStr.toString();
    }

    /**
     * Callback called upon each environment variation
     */
    public void onEnvironmentChanged() {

    }

    /**
     * Callback called every time the vacuum moves
     */
    public void onVacuumMoved() {

    }
}
