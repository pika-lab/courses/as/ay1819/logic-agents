package it.unibo.as.logicagents;

/**
 * This is both a sensor and an actuator artifact.
 * It lets an agent mode awhile measuring how much it moved since the last it checked
 */
public class MotorWithOdometer extends Artefact {

    private int steps = 0;

    public MotorWithOdometer(VacuumEnvironment environment) {
        super(environment);
    }

    /**
     * Tries moving the agent toward a particular relative direction.
     * The agent may actually fail moving in case the requested movement would put it outside the environment.
     * In any case, this method returns <code>true</code>
     *
     * @param directionName is a {@link String} contaning the case-insensitive name of one value form the {@link Direction} ones
     * @return always <code>true</code>
     * @throws Exception
     */
    public boolean move(String directionName) throws Exception {
        final Direction direction = Direction.valueOf(directionName.toUpperCase());
        final Bearing bearing = getEnvironment().getVacuumBearing();
        final Position position = getEnvironment().getVacuumPosition();

        final Position nextPosition = null; // TODO compute
        final Bearing nextBearing = null; // TODO compute

        getEnvironment().setVacuumBearing(nextBearing);

        if (getEnvironment().isWithin(nextPosition)) {
            getEnvironment().setVacuumPosition(nextPosition);
            steps++;
        }

        return true;
    }

    /**
     * Counts the amount of times the agent actually moved since the last invocation of this method
     * @return a non-negative <code>int</code>
     * @throws Exception
     */
    public int countSteps() throws Exception {
        final int result = steps;
        steps = 0;
        return result;
    }
}
