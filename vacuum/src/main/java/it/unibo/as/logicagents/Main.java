package it.unibo.as.logicagents;

import alice.tuprolog.InvalidTheoryException;
import alice.tuprolog.MalformedGoalException;
import alice.tuprolog.Prolog;
import alice.tuprolog.Theory;
import alice.tuprolog.event.ExceptionEvent;
import alice.tuprolog.event.OutputEvent;
import alice.tuprolog.lib.InvalidObjectIdException;

import java.io.IOException;
import java.io.InputStream;
import java.util.stream.IntStream;

public class Main {

    public static void main(String[] args) throws InvalidTheoryException, MalformedGoalException, IOException, InvalidObjectIdException {

        Theory theory = new Theory(Main.class.getResourceAsStream("/Vacuum.pl"));
        theory.append(new Theory(Main.class.getResourceAsStream("/Utils.pl")));

        Prolog agent = createEngineWithTheory(theory);

        VacuumEnvironment environment = new VacuumEnvironment(20, 10) {

            private int frames;

            @Override
            protected void onInit() { // Add some dirty at random
                IntStream.range(0, getHeight() * getWidth())
                        .forEach(i -> this.setRandomlyDirty());
            }

            @Override
            public void onVacuumMoved() { // Print the environment everytime the vacuum moves
                System.out.printf("### Frame %d ############\n", frames++);
                System.out.println(this.toString());
                try {
                    Thread.sleep(400);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        Artefact motor = new MotorWithOdometer(environment);
        Artefact scanner = new DirtyScanner(environment);
        Artefact sucker = new Sucker(environment);

        motor.attachTo(agent, "motor");
        scanner.attachTo(agent, "scanner");
        sucker.attachTo(agent, "sucker");

        agent.solve("start(exploring).");
    }

    public static Prolog createEngineWithTheory(InputStream theory) throws InvalidTheoryException, IOException {
        return createEngineWithTheory(new Theory(theory));
    }

    public static Prolog createEngineWithTheory(String theory) throws InvalidTheoryException {
        return createEngineWithTheory(new Theory(theory));
    }

    public static Prolog createEngineWithTheory(Theory theory) throws InvalidTheoryException {
        Prolog engine = new Prolog();
        engine.setTheory(theory);
        engine.addOutputListener(Main::printOutput);
        engine.addExceptionListener(Main::printError);
        return engine;
    }

    private static void printOutput(OutputEvent object) {
        System.out.print(cleanUpMessage(object.getMsg()));
    }

    private static void printError(ExceptionEvent object) {
        System.err.print(cleanUpMessage(object.getMsg()));
    }

    private static String cleanUpMessage(String msg) {
        if (msg.startsWith("'") && msg.endsWith("'")) {
            return msg.substring(1, msg.length() - 1);
        }
        return msg;
    }
}
