package it.unibo.as.logicagents;

/**
 * This is a sensor artifact, detecting wheter the environment position the agent is laying on is dirty or not
 */
public class DirtyScanner extends Artefact {

    public DirtyScanner(VacuumEnvironment environment) {
        super(environment);
    }

    public String scan() throws Exception {
        final Position position = getEnvironment().getVacuumPosition();

        throw new IllegalStateException("implement me");

//        if (/* there is some dirty @ position*/) {
//            return "dirty";
//        }
//
//        return "clean";
    }
}
