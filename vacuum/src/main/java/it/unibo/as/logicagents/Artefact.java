package it.unibo.as.logicagents;

import alice.tuprolog.Prolog;
import alice.tuprolog.Struct;
import alice.tuprolog.lib.InvalidObjectIdException;
import alice.tuprolog.lib.OOLibrary;

import java.util.Objects;

/**
 * Base class for all artifacts
 */
public abstract class Artefact {
    private final VacuumEnvironment environment;

    protected Artefact(VacuumEnvironment environment) {
        this.environment = Objects.requireNonNull(environment);
    }

    protected VacuumEnvironment getEnvironment() {
        return environment;
    }

    /**
     * Attach this artifact to an agent
     * @param agent the agent this artifact must be attached to (it must be a {@link Prolog} instace which loaded a {@link OOLibrary} instance)
     * @param name the name of the atom this artifact should be accessible by, within <code>agent</code>'s theory
     * @throws InvalidObjectIdException in case <code>name</code> is not a valid atom
     */
    public void attachTo(Prolog agent, String name) throws InvalidObjectIdException {
        final OOLibrary library = (OOLibrary) agent.getLibrary(OOLibrary.class.getName());
        library.register(new Struct(Objects.requireNonNull(name)), this);
    }
}
