package it.unibo.as.logicagents;

import java.util.Random;

/**
 * Orientation of an agent w.r.t. the absolute reference system of it's environment
 */
public enum Bearing {
    NORTH("↑"),
    WEST("←"),
    SOUTH("↓"),
    EAST("→");

    private static Random RAND = new Random();

    public static Bearing random() {
        return Bearing.values()[RAND.nextInt(4)];
    }

    private String representation;

    Bearing(String representation) {
        this.representation = representation;
    }

    private Bearing left() {
        return Bearing.values()[(this.ordinal() + 1) % 4];
    }

    private Bearing right() {
        if (this.ordinal() > 0) {
            return Bearing.values()[(this.ordinal()) - 1 % 4];
        }
        return EAST;
    }

    /**
     * Creates a novel {@link Bearing} by rotating the current one toward some relative {@link Direction}
     * @param direction is the {@link Direction} this {@link Bearing} should be rotated towards
     * @return a new {@link Bearing} instance
     */
    public Bearing rotate(Direction direction) {
        switch (direction) {
            case FORWARD: return this;
            case LEFT: return left();
            case RIGHT: return right();
            case BACKWARD: return back();
            default: throw new IllegalArgumentException();
        }
    }

    private Bearing back() {
        return Bearing.values()[(this.ordinal() + 2) % 4];
    }

    @Override
    public String toString() {
        return representation;
    }
}
