start(exploring) :- !,
    store(current_position, (0, 0)),
    store(facing, top),
    move_on(exploring).

move_on(Mode) :- !,
    move(forward),
    count_steps(1) ->
        clean_if_needed,
        update_position_and_facing(forward),
        move_on(Mode).
    ;
        on_hit_border(Mode).

on_hit_border(exploring) :- ! /* Do something here */.

on_hit_border(cleaning) :- ! /* Do something here */.

update_position_and_facing(Direction) :- !,
    get(current_position, (X, Y)),
    get(facing, Facing),
    estimate_next_facing(Facing, Direction, NewFacing),
    update(facing, NewFacing),
    estimate_next_position(X, Y, NewFacing, X1, Y1),
    update(current_position, (X1, Y1)),
    log_current_position(X1, Y1),
    println(["I believe I''m facing ", NewFacing]).


clean_if_needed :-
    get(current_position, (X, Y)),
    scan(dirty),
    println(["It''s dirty in (", X, ", ", Y, "): sucking up"]),
    clean_up.
clean_if_needed.