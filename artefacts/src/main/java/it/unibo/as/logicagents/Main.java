package it.unibo.as.logicagents;

import alice.tuprolog.InvalidTheoryException;
import alice.tuprolog.MalformedGoalException;
import alice.tuprolog.Prolog;
import alice.tuprolog.Theory;
import alice.tuprolog.event.ExceptionEvent;
import alice.tuprolog.event.OutputEvent;
import alice.tuprolog.lib.InvalidObjectIdException;

import java.io.IOException;
import java.io.InputStream;

public class Main {
    public static void main(String[] args) throws InvalidTheoryException, MalformedGoalException, IOException, InvalidObjectIdException {

        Prolog agent = createEngineWithTheory(Main.class.getResourceAsStream("/Thermostat.pl"));
        Prolog environment = createEngineWithTheory("temp(25).");

        Artefact sensor = new TemperatureSensor(environment);
        Artefact actuator = new TemperatureActuator(environment);

        sensor.attachTo(agent, "sensor");
        actuator.attachTo(agent, "actuator");

        agent.solve("start.");
    }

    public static Prolog createEngineWithTheory(InputStream theory) throws InvalidTheoryException, IOException {
        return createEngineWithTheory(new Theory(theory));
    }

    public static Prolog createEngineWithTheory(String theory) throws InvalidTheoryException {
        return createEngineWithTheory(new Theory(theory));
    }

    public static Prolog createEngineWithTheory(Theory theory) throws InvalidTheoryException {
        Prolog engine = new Prolog();
        engine.setTheory(theory);
        engine.addOutputListener(Main::printOutput);
        engine.addExceptionListener(Main::printError);
        return engine;
    }

    private static void printOutput(OutputEvent object) {
        System.out.print(cleanUpMessage(object.getMsg()));
    }

    private static void printError(ExceptionEvent object) {
        System.err.print(cleanUpMessage(object.getMsg()));
    }

    private static String cleanUpMessage(String msg) {
        if (msg.startsWith("'") && msg.endsWith("'")) {
            return msg.substring(1, msg.length() - 1);
        }
        return msg;
    }
}
