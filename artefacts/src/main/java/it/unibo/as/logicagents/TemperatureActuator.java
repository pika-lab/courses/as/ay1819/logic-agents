package it.unibo.as.logicagents;

import alice.tuprolog.Prolog;
import alice.tuprolog.SolveInfo;

public class TemperatureActuator extends Artefact {

    public TemperatureActuator(Prolog environment) {
        super(environment);
    }

    public boolean setTemperature(int value) throws Exception {
        String goal = String.format(
                "retract(temp(_)), " +
                "assert(temp(%d)), !.",
                value
        );

        System.out.printf("Setting temperature to: %d °C\n", value);
        SolveInfo query = getEnvironment().solve(goal);
        return query.isSuccess();
    }
}
