package it.unibo.as.logicagents;

import alice.tuprolog.Prolog;
import alice.tuprolog.Number;
import alice.tuprolog.SolveInfo;

public class TemperatureSensor extends Artefact {

    public TemperatureSensor(Prolog environment) {
        super(environment);
    }

    public int getTemperature() throws Exception {
        SolveInfo query = getEnvironment().solve("temp(X).");
        if (query.isSuccess()) {
            Number temperature = (Number) query.getTerm("X");
            System.out.printf("Perceiving temperature: %s °C\n", temperature.intValue());
            return temperature.intValue();
        } else {
            return -273; // 0 K
        }
    }
}
