start :- check_temperature.

check_temperature :-
    sensor <- getTemperature returns X,
    X >= 22, !,
    println(["Temperature too high: ", X, ", cooling down"]),
    T is X - 1,
    change_temperature(T).
check_temperature :-
    sensor <- getTemperature returns X,
    X =< 18, !, T is X + 1,
    println(["Temperature too low: ", X, ", warming up"]),
    change_temperature(T).
check_temperature :-
    sensor <- getTemperature returns X,
    X > 18, X < 22,
    println(["Temperature is ok: ", X, ", I''m done"]).

change_temperature(X) :-
    actuator <- setTemperature(X),
    check_temperature.


println([]) :- nl.
println([X | Xs]) :- write(X), println(Xs).